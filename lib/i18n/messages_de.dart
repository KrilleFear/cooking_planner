// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a de locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'de';

  static m0(count) => "${count} Zutaten";

  static m1(year, month, day, weekDay) =>
      "${weekDay} (${day}.${month}.${year})";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function>{
        "3 Tomatos\n10 Potatos\n12 Bananas":
            MessageLookupByLibrary.simpleMessage(
                "3 Tomaten\n10 Kartoffeln\n12 Bananen"),
        "About": MessageLookupByLibrary.simpleMessage("Über"),
        "Add meal": MessageLookupByLibrary.simpleMessage("Mahlzeit hinzufügen"),
        "Add some items by tapping on the + button.":
            MessageLookupByLibrary.simpleMessage(
                "Füge ein paar Einträge hinzu, indem du auf das + tippst."),
        "Add some recipes by tapping on the + button.":
            MessageLookupByLibrary.simpleMessage(
                "Füge ein paar Rezepte hinzu, indem du auf das + tippst."),
        "Are you sure?":
            MessageLookupByLibrary.simpleMessage("Bist du sicher?"),
        "Close": MessageLookupByLibrary.simpleMessage("Schließen"),
        "Confirm": MessageLookupByLibrary.simpleMessage("Bestätigen"),
        "Connect": MessageLookupByLibrary.simpleMessage("Verbinden"),
        "Edit": MessageLookupByLibrary.simpleMessage("Ändern"),
        "Friday": MessageLookupByLibrary.simpleMessage("Freitag"),
        "Grocery List": MessageLookupByLibrary.simpleMessage("Einkaufsliste"),
        "Help": MessageLookupByLibrary.simpleMessage("Hilfe"),
        "Ingredients (One per line)":
            MessageLookupByLibrary.simpleMessage("Zutaten (Eine pro Zeile)"),
        "Instructions": MessageLookupByLibrary.simpleMessage("Anweisungen"),
        "License": MessageLookupByLibrary.simpleMessage("Lizenz"),
        "Loading... Please wait.":
            MessageLookupByLibrary.simpleMessage("Lade ... Bitte warten."),
        "Meal plan": MessageLookupByLibrary.simpleMessage("Essensplan"),
        "Monday": MessageLookupByLibrary.simpleMessage("Montag"),
        "New entry": MessageLookupByLibrary.simpleMessage("Neuer Eintrag"),
        "New recipe": MessageLookupByLibrary.simpleMessage("Neues Rezept"),
        "Nextcloud connected":
            MessageLookupByLibrary.simpleMessage("Nextcloud verbunden"),
        "Nextcloud synchronisation":
            MessageLookupByLibrary.simpleMessage("Nextcloud-Synchronisation"),
        "Password": MessageLookupByLibrary.simpleMessage("Passwort"),
        "Plan": MessageLookupByLibrary.simpleMessage("Plan"),
        "Please enter a title for the receipt":
            MessageLookupByLibrary.simpleMessage(
                "Bitte gib einen Titel für das Rezept ein"),
        "Recipe title": MessageLookupByLibrary.simpleMessage("Rezept-Titel"),
        "Recipes": MessageLookupByLibrary.simpleMessage("Rezepte"),
        "Saturday": MessageLookupByLibrary.simpleMessage("Samstag"),
        "Search": MessageLookupByLibrary.simpleMessage("Suchen"),
        "Server": MessageLookupByLibrary.simpleMessage("Server"),
        "Settings": MessageLookupByLibrary.simpleMessage("Einstellungen"),
        "Source code": MessageLookupByLibrary.simpleMessage("Quellcode"),
        "Sunday": MessageLookupByLibrary.simpleMessage("Sonntag"),
        "Thursday": MessageLookupByLibrary.simpleMessage("Donnerstag"),
        "Tuesday": MessageLookupByLibrary.simpleMessage("Dienstag"),
        "Username": MessageLookupByLibrary.simpleMessage("Benutzername"),
        "Wednesday": MessageLookupByLibrary.simpleMessage("Mittwoch"),
        "You may need to create a one time password for this app.":
            MessageLookupByLibrary.simpleMessage(
                "Du brauchst ggbf. ein Einmal-Passwort für diese App."),
        "countIngredients": m0,
        "formatDate": m1
      };
}
