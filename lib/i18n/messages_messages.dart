// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a messages locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'messages';

  static m0(count) => "${count} ingredients";

  static m1(year, month, day, weekDay) =>
      "${weekDay} (${year}-${month}-${day})";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function>{
        "3 Tomatos\n10 Potatos\n12 Bananas":
            MessageLookupByLibrary.simpleMessage(
                "3 Tomatos\n10 Potatos\n12 Bananas"),
        "About": MessageLookupByLibrary.simpleMessage("About"),
        "Add meal": MessageLookupByLibrary.simpleMessage("Add meal"),
        "Add some items by tapping on the + button.":
            MessageLookupByLibrary.simpleMessage(
                "Add some items by tapping on the + button."),
        "Add some recipes by tapping on the + button.":
            MessageLookupByLibrary.simpleMessage(
                "Add some recipes by tapping on the + button."),
        "Are you sure?": MessageLookupByLibrary.simpleMessage("Are you sure?"),
        "Close": MessageLookupByLibrary.simpleMessage("Close"),
        "Confirm": MessageLookupByLibrary.simpleMessage("Confirm"),
        "Connect": MessageLookupByLibrary.simpleMessage("Connect"),
        "Edit": MessageLookupByLibrary.simpleMessage("Edit"),
        "Friday": MessageLookupByLibrary.simpleMessage("Friday"),
        "Grocery List": MessageLookupByLibrary.simpleMessage("Grocery List"),
        "Help": MessageLookupByLibrary.simpleMessage("Help"),
        "Ingredients (One per line)":
            MessageLookupByLibrary.simpleMessage("Ingredients (One per line)"),
        "Instructions": MessageLookupByLibrary.simpleMessage("Instructions"),
        "License": MessageLookupByLibrary.simpleMessage("License"),
        "Loading... Please wait.":
            MessageLookupByLibrary.simpleMessage("Loading... Please wait."),
        "Meal plan": MessageLookupByLibrary.simpleMessage("Meal plan"),
        "Monday": MessageLookupByLibrary.simpleMessage("Monday"),
        "New entry": MessageLookupByLibrary.simpleMessage("New entry"),
        "New recipe": MessageLookupByLibrary.simpleMessage("New recipe"),
        "Nextcloud connected":
            MessageLookupByLibrary.simpleMessage("Nextcloud connected"),
        "Nextcloud synchronisation":
            MessageLookupByLibrary.simpleMessage("Nextcloud synchronisation"),
        "Password": MessageLookupByLibrary.simpleMessage("Password"),
        "Plan": MessageLookupByLibrary.simpleMessage("Plan"),
        "Please enter a title for the receipt":
            MessageLookupByLibrary.simpleMessage(
                "Please enter a title for the receipt"),
        "Recipe title": MessageLookupByLibrary.simpleMessage("Recipe title"),
        "Recipes": MessageLookupByLibrary.simpleMessage("Recipes"),
        "Saturday": MessageLookupByLibrary.simpleMessage("Saturday"),
        "Search": MessageLookupByLibrary.simpleMessage("Search"),
        "Server": MessageLookupByLibrary.simpleMessage("Server"),
        "Settings": MessageLookupByLibrary.simpleMessage("Settings"),
        "Source code": MessageLookupByLibrary.simpleMessage("Source code"),
        "Sunday": MessageLookupByLibrary.simpleMessage("Sunday"),
        "Thursday": MessageLookupByLibrary.simpleMessage("Thursday"),
        "Tuesday": MessageLookupByLibrary.simpleMessage("Tuesday"),
        "Username": MessageLookupByLibrary.simpleMessage("Username"),
        "Wednesday": MessageLookupByLibrary.simpleMessage("Wednesday"),
        "You may need to create a one time password for this app.":
            MessageLookupByLibrary.simpleMessage(
                "You may need to create a one time password for this app."),
        "countIngredients": m0,
        "formatDate": m1
      };
}
