import 'package:nextcloud/nextcloud.dart';
/* Cooking Planner
* Copyright (C) 2020  Christian Pauly
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* 
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class NextcloudCredentials {
  String server;
  String username;
  String password;

  NextcloudCredentials({this.server, this.username, this.password});

  NextcloudCredentials.fromJson(Map<String, dynamic> json) {
    server = json['server'];
    username = json['username'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['server'] = this.server;
    data['username'] = this.username;
    data['password'] = this.password;
    return data;
  }

  NextCloudClient get nextCloudClient => NextCloudClient(
        server,
        username,
        password,
      );
}
