/* Cooking Planner
* Copyright (C) 2020  Christian Pauly
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* 
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import 'package:cooking_planner/components/custom_bottom_bar.dart';
import 'package:cooking_planner/i18n/i18n.dart';
import 'package:cooking_planner/model/cooking_plannert.dart';
import 'package:cooking_planner/views/recipe_edit_view.dart';
import 'package:cooking_planner/views/settings_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RecipesView extends StatelessWidget {
  final TextEditingController searchController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        title: Container(
          height: 44,
          margin: EdgeInsets.all(8),
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: Colors.white.withOpacity(0.66),
          ),
          child: TextField(
            controller: searchController,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(10),
              prefixIcon: Icon(Icons.search),
              hintText: I18n.of(context).search,
              border: InputBorder.none,
            ),
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () => Navigator.of(context).push(
              CupertinoPageRoute(
                builder: (c) => SettingsView(),
              ),
            ),
          )
        ],
      ),
      body: FutureBuilder<bool>(
        future: CookingPlanner().isReady,
        builder: (BuildContext context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }
          final recipes = CookingPlanner().recipes;
          return recipes.isEmpty
              ? Center(
                  child: Text(I18n.of(context).addSomeRecipesBy),
                )
              : RefreshIndicator(
                  onRefresh: () => CookingPlanner().refreshFromNextcloud(),
                  child: StreamBuilder<Object>(
                      stream: CookingPlanner().onRecipesUpdate.stream,
                      builder: (context, snapshot) {
                        String searchText = '';
                        return StatefulBuilder(builder: (context, setState) {
                          searchController.addListener(() {
                            setState(() => searchText = searchController.text);
                          });
                          return ListView.builder(
                            itemCount: recipes.length,
                            itemBuilder: (BuildContext context, int i) =>
                                recipes[i]
                                        .title
                                        .toLowerCase()
                                        .contains(searchText.toLowerCase())
                                    ? Card(
                                        child: ListTile(
                                          title: Text(recipes[i].title),
                                          subtitle: Text(
                                            I18n.of(context).countIngredients(
                                              recipes[i]
                                                  .ingredients
                                                  .length
                                                  .toString(),
                                            ),
                                          ),
                                          onTap: () =>
                                              Navigator.of(context).push(
                                            CupertinoPageRoute(
                                              builder: (c) => RecipeEditView(
                                                recipeId: recipes[i].id,
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    : Container(),
                          );
                        });
                      }),
                );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.of(context).push(
          CupertinoPageRoute(
            builder: (c) => RecipeEditView(),
          ),
        ),
      ),
      bottomNavigationBar: CustomBottomBar(0),
    );
  }
}
