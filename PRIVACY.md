# Privacy Policy

The app Cooking Planner is only storing data the user has created. These data are:
- Recipes
- Meals
- Tasks

The user can choice where the app stores the data. This can be:
- On the phone encrypted in a secure storage
- On the user's Nextcloud instance in the file: `/cooking-planner-backup.json`

By default Cooking Planner is storing the data on the phone. Cooking Planner is not responsible for the security of the user's Nextcloud instance.
